# vue.calendar

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

## Technologies used
- Vue.js
- Vuex
- Vue Router
- Moment.js
- Vuelidate
- Sass
  
## Editor extensions recommended
- Editor config
- ESLint

## Notes
The described application required a routing, user and content management systems to be created and implemented. All of these are present in the provided project files aswell as the required Vue store actions associated with application behaviour except the modal component for listing the events and deleting individual ones. The 'Active item' would be populated with the filtered event set retrieved from the Day panel component and passed to a specific store action that will make the proper mutations in the calendar and persistentData store modules. The described search works on the same principal as the way we retrieve information but instead of month we will filter by title the user events array.
Unit testing plugins for ESLint/Jest have also been included.