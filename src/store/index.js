import Vue from 'vue';
import Vuex from 'vuex';
import initialState from './initialState';
import persistentStore from './persistentStore';
import persistentData from './modules/persistentData';
// eslint-disable-next-line import/no-cycle
import auth from './modules/auth';
// eslint-disable-next-line import/no-cycle
import calendar from './modules/calendar';

Vue.use(Vuex);

const options = {
  plugins: [
    persistentStore.plugin
  ],
  modules: {
    persistentData: persistentData(initialState.persistentData),
    auth: auth(initialState.auth),
    calendar: calendar(initialState.calendar)
  }
};

/**
 * @description Reset stores
 * @param store
 */
export const resetStores = (store) => {
  store.dispatch('auth/reset');
  store.dispatch('calendar/reset');
  store.dispatch('persistentData/reset');
};

export default new Vuex.Store(options);
