// eslint-disable-next-line import/no-cycle
import router from '../../router';
// eslint-disable-next-line import/no-cycle
import store, { resetStores } from '..';
import initialState from '../initialState';
import { isObj } from '../../utils';

/**
 * @description Is valid
 * @param auth
 * @returns {boolean}
 */
const isValid = (auth) => isObj(auth)
  && isObj(auth.user)
  && isObj(auth.redirects);

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = initialState => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial auth state');
  }

  const { user, redirects } = initialState;
  return {
    user,
    redirects
  };
};

/**
 * @description Getters
 * @type {*}
 */
export const getters = {
  user: ({ user }) => user,
  permissions: ({ permissions }) => permissions,
  redirects: ({ redirects }) => redirects,
  isLogged: ({ user }) => user.id
};

const actions = {
  login: ({ state, commit }, data) => {
    const { user, persist = false } = data;
    const { firstName, lastName, id } = user;

    const nextUser = {
      id,
      firstName,
      lastName
    };

    const nextAuth = {
      ...state,
      user: nextUser
    };

    if (persist) {
      store.dispatch('persistentData/storeUser', nextUser)
        .catch((err) => err);
    }

    commit('SET', nextAuth);

    return router.replace({ name: 'calendar' });
  },
  logout: () => {
    store.dispatch('auth/reset');
    store.dispatch('calendar/reset');

    return router.replace({ name: 'auth' });
  },
  reset: ({ commit }) => (
    commit('SET', initialState.auth)
  ),
  resetAll: () => resetStores(store),
  set: ({ commit }, auth) => {
    commit('SET', auth);
  }
};

const mutations = {
  SET(state, auth) {
    /* eslint-disable no-param-reassign */
    state.user = auth.user;
    state.permissions = auth.permissions;
    state.redirects = auth.redirects;
  }
};

export default initialState => ({
  namespaced: true,
  state: initState(initialState),
  getters,
  actions,
  mutations
});
