// eslint-disable-next-line import/no-cycle
import store from '..';
import initialState from '../initialState';
import { isArr, isObj } from '../../utils';

/**
 * @description Is valid
 * @param banners
 * @returns {boolean}
 */
const isValid = (calendar) => {
  const { date, events, schedule, activeItem } = calendar;

  return isObj(calendar)
    && isObj(date)
    && isArr(events)
    && isObj(schedule)
    && isObj(activeItem);
};

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = initialState => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial calendar state');
  }

  const { date, events, schedule, activeItem } = initialState;
  return {
    date,
    events,
    schedule,
    activeItem
  };
};

/**
 * @description Getters
 * @type {*}
 */
export const getters = {
  date: ({ date }) => date,
  events: ({ events }) => events,
  schedule: ({ schedule }) => schedule,
  activeItem: ({ activeItem }) => activeItem
};

const actions = {
  getEvents: ({ state, commit }, { id }) => {
    const events = store.getters['persistentData/events'];

    const nextEvents = events.filter(
      (event) => event.owner === id
    );

    const nextCalendar = {
      ...state,
      events: nextEvents
    };

    return commit('SET', nextCalendar);
  },
  createEvent: ({ state, commit }, event) => {
    const nextEvents = [
      ...state.events,
      event
    ];

    const nextCalendar = {
      ...state,
      events: nextEvents
    };

    store.dispatch('persistentData/storeEvent', event)
      .catch((err) => err);

    return commit('SET', nextCalendar);
  },
  deleteEvent: ({ state, commit }, { id }) => {
    const nextEvents = state.events.filter(
      (event) => event.id !== id
    );

    const nextCalendar = {
      ...state,
      events: nextEvents
    };

    store.dispatch('persistentData/dropEvent', id)
      .catch((err) => err);

    return commit('SET', nextCalendar);
  },
  setCurrentDate: ({ state, commit }, data) => {
    const { day, month, year } = data;

    const nextDate = {
      day,
      month,
      year
    };

    const nextCalendar = {
      ...state,
      date: nextDate
    };

    return commit('SET', nextCalendar);
  },
  setCalendarDate: ({ state, commit }, data) => {
    const { day, month, year } = data;

    const nextDate = {
      day,
      month,
      year
    };

    const nextSchedule = {
      date: nextDate,
      items: []
    };

    const nextCalendar = {
      ...state,
      schedule: nextSchedule
    };

    return commit('SET', nextCalendar);
  },
  reset: ({ commit }) => (
    commit('SET', initialState.calendar)
  ),
  set: ({ commit }, calendar) => {
    commit('SET', calendar);
  }
};

const mutations = {
  SET(state, calendar) {
    /* eslint-disable no-param-reassign */
    state.date = calendar.date;
    state.events = calendar.events;
    state.schedule = calendar.schedule;
    state.activeItem = calendar.activeItem;
  }
};

export default initialState => ({
  namespaced: true,
  state: initState(initialState),
  getters,
  actions,
  mutations
});
