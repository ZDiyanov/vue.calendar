import initialState from '../initialState';
import { isArr, isObj } from '../../utils';

/**
 * @description Is valid
 * @param config
 * @returns {boolean}
 */
const isValid = (persistentData) => isObj(persistentData)
  && isArr(persistentData.users)
  && isArr(persistentData.events);

/**
 * @description Init state
 * @param initialState
 * @returns {*}
 */
const initState = initialState => {
  if (!isValid(initialState)) {
    throw Error('Invalid initial persistent data state');
  }

  const { users, events } = initialState;
  return {
    users,
    events
  };
};

/**
 * @description Getters
 * @type {*}
 */
export const getters = {
  users: ({ users }) => users,
  events: ({ events }) => events
};

const actions = {
  storeUser: ({ state, commit }, user) => {
    const nextUsers = [
      ...state.users,
      user
    ];

    const nextPersistentData = {
      ...state,
      users: nextUsers
    };

    return commit('SET', nextPersistentData);
  },
  storeEvent: ({ state, commit }, event) => {
    const nextEvents = [
      ...state.events,
      event
    ];

    const nextPersistentData = {
      ...state,
      events: nextEvents
    };

    return commit('SET', nextPersistentData);
  },
  dropEvent: ({ state, commit }, id) => {
    const nextEvents = state.events.filter(
      (event) => event.id !== id
    );

    const nextPersistentData = {
      ...state,
      events: nextEvents
    };

    return commit('SET', nextPersistentData);
  },
  reset: ({ commit }) => (
    commit('SET', initialState.persistentData)
  ),
  set: ({ commit }, persistentData) => {
    commit('SET', persistentData);
  }
};

const mutations = {
  SET(state, persistentData) {
    /* eslint-disable no-param-reassign */
    state.users = persistentData.users;
    state.events = persistentData.events;
  }
};

export default initialState => ({
  namespaced: true,
  state: initState(initialState),
  getters,
  actions,
  mutations
});
