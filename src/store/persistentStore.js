import VuexPersistence from 'vuex-persist';
import initialState from './initialState';

const { localStorage } = window;

export default new VuexPersistence({
  storage: localStorage,
  key: 'vue_calendar',
  reducer: (state) => ({
    ...state,
    auth: {
      ...initialState.auth,
      user: {
        ...initialState.auth.user,
        id: state.auth.user.id,
        firstName: state.auth.user.firstName,
        lastName: state.auth.user.lastName
      }
    },
    persistentData: {
      ...initialState.persistentData,
      users: state.persistentData.users,
      events: state.persistentData.events
    },
    calendar: initialState.calendar
  })
});
