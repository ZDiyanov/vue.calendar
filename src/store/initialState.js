export default {
  auth: {
    user: {},
    redirects: {
      index: '/',
      unauthenticated: '/auth',
      authenticated: '/calendar'
    }
  },
  persistentData: {
    users: [],
    events: []
  },
  calendar: {
    date: {},
    events: [],
    schedule: {
      date: {},
      items: []
    },
    activeItem: {}
  }
};
