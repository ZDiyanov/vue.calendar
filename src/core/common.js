import Vue from 'vue';

import Button from '../components/common/Button';
import TextField from '../components/common/TextField';
import Dropdown from '../components/common/Dropdown';


Vue.component('btn', Button);
Vue.component('text-field', TextField);
Vue.component('dropdown', Dropdown);
