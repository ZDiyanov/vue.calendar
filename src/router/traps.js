// eslint-disable-next-line import/no-cycle
import store from '../store';
// eslint-disable-next-line import/no-cycle
import router from '.';

/**
 * @description Is matching
 * @param path
 * @param urlParts
 * @returns {boolean}
 */
const isMatching = (path, ...urlParts) => {
  let hasMatch = false;

  urlParts.forEach(urlPart => {
    if (hasMatch) {
      return false;
    }
    hasMatch = path.indexOf(urlPart) > -1;
  });

  return hasMatch;
};

/**
 * @description Requires authentication
 * @param to
 * @returns {boolean}
 */
const requiresAuth = (to) => to.matched.some(record => record.meta.auth);

/**
 * @description Is authenticated route
 * @param path
 * @returns {boolean}
 */
const isAuthRoute = (path) => isMatching(path, 'auth');

/**
 * @description Is logged
 * @returns {*}
 */
const isLogged = () => store.getters['auth/isLogged'];

/**
 * @description Get redirects
 * @returns {*}
 */
const getRedirects = () => (
  router.app.$store.getters['auth/redirects']
);

/**
 * @description Redirect unauthenticated
 * @param to
 * @param from
 * @param next
 */
export const redirectUnauthenticated = (to, from, next) => {
  const { index, unauthenticated, authenticated } = getRedirects();

  if (requiresAuth(to) && !isLogged()) {
    next(unauthenticated);
  } else if (to.path === index) {
    next(authenticated);
  } else {
    next();
  }
};

/**
 * @description Redirect authenticated
 * @param to
 * @param from
 * @param next
 */
export const redirectAuthenticated = (to, from, next) => {
  const { index } = getRedirects();

  if (isAuthRoute(to.path) && isLogged()) {
    return next(index);
  }

  next();
};

/**
 * @description Redirect from index
 * @param to
 * @param from
 * @param next
 */
export const redirectFromIndex = (to, from, next) => {
  const { index, authenticated } = getRedirects();

  if (to.path === index) {
    return next(authenticated);
  }
  next();
};
