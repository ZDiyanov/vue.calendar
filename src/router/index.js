import Vue from 'vue';
import Router from 'vue-router';
import multiguard from 'vue-router-multiguard';
// eslint-disable-next-line import/no-cycle
import {
  redirectUnauthenticated,
  redirectAuthenticated,
  redirectFromIndex
} from './traps';
import home from './routes/home';
import notFound from './routes/notFound';
import authPage from './routes/auth';
import calendar from './routes/calendar';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: '/',
  saveScrollPosition: true,
  routes: [
    ...home,
    ...notFound,
    ...authPage,
    ...calendar
  ]
});

// configure traps
router.beforeEach(multiguard([
  redirectUnauthenticated,
  redirectAuthenticated,
  redirectFromIndex
]));

export default router;
