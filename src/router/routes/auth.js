export default [
  {
    path: '/auth',
    name: 'auth',
    meta: {
      loadState: true,
      auth: false,
      isAuthPage: true,
    },
    // route level code-splitting
    component: () => import(/* webpackChunkName: "auth" */ '../../views/auth/Index'),
  }
];
