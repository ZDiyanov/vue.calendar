export default [
  {
    path: '/calendar',
    name: 'calendar',
    meta: {
      loadState: true,
      auth: true,
      isAuthPage: false
    },
    // route level code-splitting
    component: () => import(/* webpackChunkName: "calendar" */ '../../views/calendar/Index'),
  },
  {
    path: '/calendar/:id',
    name: 'event-details',
    meta: {
      loadState: true,
      auth: true,
      isAuthPage: false
    },
    // route level code-splitting
    component: () => import(/* webpackChunkName: "event-details" */ '../../views/calendar/Single'),
  }
];
