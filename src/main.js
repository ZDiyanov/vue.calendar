import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import 'normalize.css';
import './plugins/meta';
import './plugins/vuelidate';
import './plugins/moment';
import './core/common';

// customize config
Vue.config.productionTip = false;

// instantiate app
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
