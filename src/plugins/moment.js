import Vue from 'vue';
import VueMoment from 'vue-moment';
import moment from 'moment';

moment.updateLocale('en', {
  week: {
    dow: 1,
  },
});

Vue.use(VueMoment, {
  moment
});
